# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

### Stage 1: Setup base python with requirements
FROM python:3.8-slim-buster AS base

# Add necessary packages to system
RUN apt-get update && apt-get install -y \
  build-essential \
  libpq-dev \
  libmagic1 \
  git

COPY requirements/base.txt /tmp/requirements.txt

RUN python3 -m venv /opt/virtualenv \
  && . /opt/virtualenv/bin/activate \
  && pip install -r /tmp/requirements.txt

ENV PATH="/opt/virtualenv/bin:$PATH"

# Set up application
COPY . /opt/zsnl_consumer_integrations
WORKDIR /opt/zsnl_consumer_integrations

## Stage 2: Production build
FROM python:3.8-slim-buster AS production

ENV OTAP=production
ENV PYTHONUNBUFFERED=on
ENV PATH="/opt/virtualenv/bin:$PATH"

RUN apt-get update && apt-get install -y libmagic1 libpq5

# Set up application
COPY --from=base /opt /opt/

WORKDIR /opt/zsnl_consumer_integrations
RUN pip3 install -e .

## Stage 3: QA environment
FROM base AS quality-and-testing

ENV OTAP=test

COPY requirements/test.txt /tmp/requirements-test.txt
RUN pip3 install -r /tmp/requirements-test.txt

RUN  mkdir -p /root/test_result \
  && bin/git-hooks/pre-commit -c -j /root/test_result/junit.xml

## Stage 4: Development image
FROM quality-and-testing AS development

ENV OTAP=development

COPY requirements/dev.txt /tmp/requirements-dev.txt
RUN pip3 install -r /tmp/requirements-dev.txt \
  && pip3 install -e .